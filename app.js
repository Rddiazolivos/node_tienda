// app.js
const express = require('express');
const bodyParser = require('body-parser');
// initialize our express app
const product = require('./routes/product.route');
const app = express();

//configuracion de mongoose
const mongoose = require('mongoose');
let dev_db_url = 'mongodb://someuser:abcd1234@ds253804.mlab.com:53804/products_tutorial'
var mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use('/products', product);
let port = 8081;
app.listen(port, () => {
	console.log('Servidor esta arriba y corriendo en el puerto ' + port);
})